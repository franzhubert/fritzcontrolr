#!/bin/bash

echo "input ip:"
read fb_ip
echo "input username (empty for no user):"
read fb_usr
echo "input password (empty for no password):"
read -s fb_pw

getSID(){
  location="/upnp/control/deviceconfig"
  uri="urn:dslforum-org:service:DeviceConfig:1"
  action='X_AVM-DE_CreateUrlSID'

  cn_sid=$(curl -s -k -m 5 --anyauth -u "$fb_usr:$fb_pw" "http://$fb_ip:49000$location" -H 'Content-Type: text/xml; charset="utf-8"' -H "SoapAction:$uri#$action" -d "<?xml version='1.0' encoding='utf-8'?><s:Envelope s:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/' xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'><s:Body><u:$action xmlns:u='$uri'></u:$action></s:Body></s:Envelope>" | grep "NewX_AVM-DE_UrlSID" | awk -F">" '{print $2}' | awk -F"<" '{print $1}' | awk -F"=" '{print $2}')
}

echo "connecting"
getSID

echo "connected"
echo "connection SID: $cn_sid"

echo "disconnecting"
